export interface CounterState {
  counter: number;
}

export const initialeState: CounterState = {
  counter: 0,
};
