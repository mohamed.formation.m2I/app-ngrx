import { Action, createReducer, on } from '@ngrx/store';
import { CounterState, initialeState } from './counter.state';
import * as CounterAction from './counter.actions';

const counterReducer = createReducer(
  initialeState,
  on(CounterAction.increment, (state) => {
    return {
      ...state,
      counter: state.counter + 1,
    };
  }),
  on(CounterAction.decrement, (state) => {
    return {
      ...state,
      counter: state.counter - 1,
    };
  }),
  on(CounterAction.reset, (state) => {
    return {
      ...state,
      counter: 0,
    };
  })
);

export function reducer(state: CounterState, action: Action) {
  return counterReducer(state, action);
}
