import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { CounterComponent } from './component/counter/counter.component';
import { CounterButtonComponent } from './shared/counter-button/counter-button.component';
import { CounterOutputComponent } from './shared/counter-output/counter-output.component';
import { COUNTER_STATE_NAME } from './store/counter.selector';
import { reducer } from './store/counter.reducer';

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    CounterButtonComponent,
    CounterOutputComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
/*     StoreModule.forFeature(COUNTER_STATE_NAME, reducer), */
    StoreModule.forRoot(reducer),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
